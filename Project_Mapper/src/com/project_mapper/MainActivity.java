package com.project_mapper;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;

public class MainActivity extends SherlockFragmentActivity {
	
	//this are menu buttons
		final int ID_PROFIFE = Menu.FIRST;
	    final int ID_FRIENDS = Menu.FIRST + 1;
	    final int ID_SETTINGS = Menu.FIRST + 2;

	private ViewPager mViewPager;
	private TabsAdapter mTabsAdapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		mViewPager = new ViewPager(this);
		mViewPager.setId(R.id.pager);
		setContentView(mViewPager);
		
		final ActionBar bar = getSupportActionBar();
		bar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		
		mTabsAdapter = new TabsAdapter(this, mViewPager);
		mTabsAdapter.addTab(bar.newTab().setText("Main Page"), Fragment_MainPage.class, null);
		mTabsAdapter.addTab(bar.newTab().setText("My Tags"), Fragment_MyTags.class, null);

	}
	
		@Override
	    public boolean onCreateOptionsMenu(Menu menu){
	    	super.onCreateOptionsMenu(menu);
	    	//add menu option
	    	menu.add(Menu.NONE, ID_PROFIFE, 0, "Profile");
	    	menu.add(Menu.NONE, ID_FRIENDS, 0, "Friends");
	    	menu.add(Menu.NONE, ID_SETTINGS,0, "Setting");
			return true;
		}
		 //option selected
	    @Override
	    public boolean onOptionsItemSelected(MenuItem item){
	    	switch (item.getItemId()){
	    	case ID_PROFIFE:
	    		Intent profile = new Intent(MainActivity.this, Map.class);
				startActivity(profile);
				//overridePendingTransition(R.animator.push_left_in,R.animator.push_left_out);
				return true;	
	    	}
	    	return super.onOptionsItemSelected(item);
	    }
}
