package com.project_mapper;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
 
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;



import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapLongClickListener;
import com.google.android.gms.maps.GoogleMap.OnMyLocationChangeListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
 
public class Map extends FragmentActivity {
 
private GoogleMap googleHarita;
boolean stop = true;
 Marker mCurrentMarker; 
 
@Override
protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.map);
    
    if (googleHarita == null) {
        googleHarita = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.haritafragment)).getMap();
        if (googleHarita != null) {

            googleHarita.setMyLocationEnabled(true);
            googleHarita.setOnMyLocationChangeListener(new OnMyLocationChangeListener() {
				
				@Override
				public void onMyLocationChange(Location location) {
					LatLng loc = new LatLng(location.getLatitude(), location.getLongitude());	
					
					if(mCurrentMarker==null)
					{
						mCurrentMarker = googleHarita.addMarker(new MarkerOptions().position(new LatLng( loc.latitude,loc.longitude )));
					}
					if(googleHarita != null){
			        	if(stop == true)
			        	googleHarita.animateCamera(CameraUpdateFactory.newLatLngZoom(loc, 13));
			        	stop = false;
			        	
			        }
					
				}
			});
            googleHarita.setOnMapLongClickListener(new OnMapLongClickListener() {
				
				@Override
				public void onMapLongClick(LatLng arg0) {
					
					LatLng touchlocation = new LatLng(arg0.latitude,arg0.longitude);
					
					mCurrentMarker.setPosition(touchlocation);
					Toast tast = Toast.makeText(getApplicationContext(),"coordinate "+touchlocation, Toast.LENGTH_LONG);
					tast.show();
					
				}
			});
            Button click = (Button)findViewById(R.id.button1);
            click.setOnClickListener(new View.OnClickListener() 
            {
				
				@Override
				public void onClick(View v) {
					if(googleHarita.getMapType()==GoogleMap.MAP_TYPE_NORMAL)
					{
						googleHarita.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
					}
					else
					{
						googleHarita.setMapType(GoogleMap.MAP_TYPE_NORMAL);
					}
				}
			});
   	     Button deneme = (Button)findViewById(R.id.button2);
   	     final EditText edittext = (EditText) findViewById(R.id.editText1);
	   	     deneme.setOnClickListener(new View.OnClickListener() {
	   			
	   			@Override
	   			public void onClick(View v) {
	   				Toast.makeText(getApplicationContext(),edittext.getText(), Toast.LENGTH_LONG).show();
	   			}
	   		});
	   	     
	   	  Button addtag = (Button)findViewById(R.id.b_addtag);
	      addtag.setOnClickListener(new View.OnClickListener() {
		   			
		   			@Override
		   			public void onClick(View v) {
		   				Intent i = new Intent(Map.this, Add_Tag.class);
						startActivity(i);
		   			}
		   		});
            
        }
        
    }
}
}